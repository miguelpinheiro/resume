package com.example.rsum

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.view.LayoutInflaterCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavInflater
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.get
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_item_row.view.*

class RecyclerAdapter(private val names: ArrayList<String>,
                      private val descriptions: ArrayList<String>,
                      private val observations: ArrayList<String>,
                      private val representations: ArrayList<Fragment>,
                      private val details: ArrayList<Fragment>,
                      private val navHostFragment: Fragment?,
                      private val inflater: NavInflater?): RecyclerView.Adapter<RecyclerAdapter.ViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.layout_item_row, false)
        return ViewHolder(inflatedView, navHostFragment, inflater)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val name = names[position]
        val description = descriptions[position]
        val observation = observations[position]
        /*val representation = representations[position]
        val details = details[position]*/

        holder.bind(name, description, observation/*, representation, details*/)
        holder.dispose()
    }

    override fun getItemCount() = names.size

    class ViewHolder(v: View, var navHostFragment: Fragment?, var inflater: NavInflater?): RecyclerView.ViewHolder(v), View.OnClickListener{
        var view = v
        var myNavHostFragment: Fragment? = navHostFragment
        var myNavInflater: NavInflater? = null
        var name: String? = null
        var description: String? = null
        var observation: String? = null
        var representation: Fragment? = null
        var details: Fragment? = null

        fun bind(name: String, description: String, observation: String/*, representation: Fragment, details: Fragment*/) {
            this.name = name
            this.description = description
            this.observation = observation
            this.representation = representation
            this.details = details
            this.myNavHostFragment = navHostFragment
            this.myNavInflater = inflater
        }

        fun dispose() {
            view.name.text = name
            view.description.text = description
            view.observation.text = observation
        }

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            v?.setOnClickListener(View.OnClickListener {
                /*val graph = inflater!!.inflate(R.navigation.nav_graph)
                graph!!.findNode(R.id.navigation_visual)!!.id = R.id.ga_representation
                if (graph != null) {
                    myNavHostFragment!!.findNavController()!!.graph = graph
                    Log.i("debug", "Chegou aqui")
                }
                Log.i("debug", "Chegou aqui2")*/
            })
        }
    }

}