package com.example.rsum

import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.graphics.Color
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.drawable.toDrawable
import com.example.rsum.GeneticAlgorithm.GeneticAlgorithm
import com.example.rsum.GeneticAlgorithm.Individual
import com.example.rsum.GeneticAlgorithm.LAB
import com.example.rsum.GeneticAlgorithm.Population
import kotlinx.android.synthetic.main.fragment_visual.*
import kotlinx.android.synthetic.main.fragment_visual.view.*
import kotlinx.android.synthetic.main.ga_representation.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [VisualRepresentation.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [VisualRepresentation.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class VisualRepresentation : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private var red = 0
    private var green = 0
    private var blue = 0

    private var solver: GeneticAlgorithm? = null
    private var vibrator: Vibrator? = null

    var individual = Individual(0, 0, 0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var representationFragment = inflater.inflate(R.layout.fragment_visual, container, false)

        var goalIndividualPanel = representationFragment.goalIndividualPanel
        //var goalTextView = representationFragment.objetivoTextView


        var bestIndividualPanel = representationFragment.bestIndividualPanel
        //var bestIndividualTextView = representationFragment.melhorIndividuoTextView

        var redSeekBar = representationFragment.redSeekBar
        var redTextView = representationFragment.redTextView


        var greenSeekBar = representationFragment.greenSeekBar
        var greenTextView = representationFragment.greenTextView


        var blueSeekBar = representationFragment.blueSeekBar
        var blueTextView = representationFragment.blueTextView


        var timeTextView = representationFragment.timeTextView
        var adaptabilityTextView = representationFragment.adaptabilityTextView
        var generationTextView = representationFragment.generationTextView

        var simulateButton = representationFragment.simulate_button

        redSeekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                red = Math.floor(2.55 * progress).toInt()
                if (progress == 100) {
                    red += 1
                }
                redTextView.text = Integer.toString(red)
                goalIndividualPanel.setBackgroundColor(Color.rgb(red, green, blue))
                updateTextColor(objetivoTextView, red, green, blue)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })
        greenSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                green = Math.floor(2.55 * progress).toInt()
                if (progress == 100) {
                    green += 1
                }
                greenTextView.text = Integer.toString(green)
                goalIndividualPanel.setBackgroundColor(Color.rgb(red, green, blue))
                updateTextColor(objetivoTextView, red, green, blue)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })
        blueSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                blue = Math.floor(2.55 * progress).toInt()
                if (progress == 100) {
                    blue += 1
                }
                blueTextView.text = Integer.toString(blue)
                goalIndividualPanel.setBackgroundColor(Color.rgb(red, green, blue))
                updateTextColor(objetivoTextView, red, green, blue)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })

        simulateButton.setOnClickListener(View.OnClickListener {
            var population = Population(350, red, green, blue)
            if (solver?.status != AsyncTask.Status.RUNNING) {
                solver = GeneticAlgorithm(population, timeTextView, generationTextView,
                    adaptabilityTextView, bestIndividualPanel)
                solver?.execute(null, null, null)
            }
            updateTextColor(melhorIndividuoTextView, individual.color[0], individual.color[1],
                individual.color[2])
        })

        // Inflate the layout for this fragment
        return representationFragment
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    fun updateTextColor(textView: TextView, red: Int, green: Int, blue: Int) {

        if (LAB.ciede2000(LAB.fromRGB(red, green, blue, 0.0),
                LAB.fromRGB(0, 0, 0, 0.0)) < 50) {
            textView.setTextColor(Color.rgb(255, 255, 255))
        } else {
            textView.setTextColor(Color.rgb(0, 0, 0))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        var goalPanelColor = goalColorPanel?.solidColor
        var goalTextColor = objetivoTextView.currentTextColor

        var bestIndividualPanelColor = bestIndividualPanel.solidColor
        var bestIndividualTextColor = melhorIndividuoTextView.currentTextColor

        var redSeekBarProgress = redSeekBar.progress
        var redTextViewText = redTextView.text.toString()

        var greenSeekBarProgress = greenSeekBar.progress
        var greenTextViewText = greenTextView.text.toString()

        var blueSeekBarProgress = blueSeekBar.progress
        var blueTextViewText = blueTextView.text.toString()

        var time = timeTextView.text.toString()
        var adaptability = adaptabilityTextView.text.toString()
        var generation = generationTextView.text.toString()

        Log.i("goalTextColor", "goalTextColor: $goalTextColor!")

        outState.putInt("goalPanelColor", goalPanelColor ?: 0)
        outState.putInt("goalTextColor", goalTextColor)

        outState.putInt("bestIndividualPanelColor", bestIndividualPanelColor)
        outState.putInt("bestIndividualTextColor", bestIndividualTextColor)

        outState.putInt("redSeekBarProgress", redSeekBarProgress)
        outState.putString("redTextViewText", redTextViewText)

        outState.putInt("greenSeekBarProgress", greenSeekBarProgress)
        outState.putString("greenTextViewText", greenTextViewText)

        outState.putInt("blueSeekBarProgress", blueSeekBarProgress)
        outState.putString("blueTextViewText", blueTextViewText)

        outState.putString("time", time)
        outState.putString("adaptability", adaptability)
        outState.putString("generation", generation)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment VisualRepresentation.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            VisualRepresentation().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
