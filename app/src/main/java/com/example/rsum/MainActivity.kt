package com.example.rsum

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(),
    Details.OnFragmentInteractionListener, Introduction.OnFragmentInteractionListener,
    VisualRepresentation.OnFragmentInteractionListener{

    val fragment1: Fragment = Introduction()
    val fragment2: Fragment = VisualRepresentation()
    val fragment3: Fragment = Details()
    var fragmentManager = supportFragmentManager
    var navController: NavController? = null
    var backStack = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentManager = supportFragmentManager

        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, fragment1, "home").commit()
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, fragment2, "representation").hide(fragment2).commit()
        fragmentManager.beginTransaction().add(R.id.nav_host_fragment, fragment3, "detail").hide(fragment3).commit()
        backStack.add("home")

        var navController = this.findNavController(R.id.nav_host_fragment)
        var bottomNavigation = findViewById<BottomNavigationView>(R.id.navigation)

        bottomNavigation.setupWithNavController(navController)
        bottomNavigation.setOnNavigationItemSelectedListener {item ->
            onNavDestinationSelected(item, navController)
        }
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    navController.navigate(R.id.navigation_home)
                    backStack.add("home")
                    true
                }
                R.id.navigation_visual -> {
                    navController.navigate(R.id.navigation_visual)
                    backStack.add("representation")
                    true
                }
                R.id.navigation_detail -> {
                    navController.navigate(R.id.navigation_detail)
                    backStack.add("detail")
                    true
                }
                else -> {
                    false
                }
            }
        }
        this.navController = navController
    }

    override fun onBackPressed() {

        if (backStack.size > 1) {
            backStack.removeAt(backStack.size - 1)
            when (backStack[backStack.size - 1]) {
                 "home" -> {
                    navController?.navigate(R.id.navigation_home)
                }
                "representation" -> {
                    navController?.navigate(R.id.navigation_visual)
                }
                "detail" -> {
                    navController?.navigate(R.id.navigation_detail)
                }

                else -> {
                    Log.i("Fragment Not Found", "The fragment on the backStack was not found")
                }
            }
        } else {
            finishAndRemoveTask()
        }

    }

    override fun onFragmentInteraction(uri: Uri) {

    }
}
