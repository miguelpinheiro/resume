package com.example.rsum.GeneticAlgorithm

import android.widget.LinearLayout
import java.util.*
import kotlin.collections.ArrayList

class Population(val size: Int = 10, red: Int = 0, green: Int = 0, blue: Int = 0) {

    val goalColor = arrayOf(red, green, blue)
    val goalLab = LAB.fromRGB(red, green, blue, 0.0)
    val MUTATION_PROBABILITY = 10.0

    var generation = 0
    var people = ArrayList<Individual>()
    var bestIndividual = Individual(-1, -1, -1)

    init {
        for (i in 0 until size) {
            val red = Utilities.randomInt(0, 255)
            val green = Utilities.randomInt(0, 255)
            val blue = Utilities.randomInt(0, 255)

            people.add(determineFitness(Individual(red, green, blue)))
        }

        bestIndividual.fitness = 100.0
    }

    private fun determineFitness(individual: Individual): Individual {
        individual.fitness = LAB.ciede2000(individual.labColor, this.goalLab)
        return individual
    }

    fun getRandomIndividual(): Individual {
        val randomInt = Utilities.randomInt(0, people.size)
        return people[randomInt]
    }

    fun purge() {

        var survivors = ArrayList(this.people)
        var bestIndividual = this.bestIndividual

        for (i in 0 until this.size) {

            val individual = people[i]
            val randomNumber = Utilities.randomDouble(0.0, 100.0)
            val fitness = individual.fitness

            if (bestIndividual != null) {
                if (fitness < bestIndividual.fitness) {
                    this.bestIndividual = individual
                }
            } else {
                bestIndividual = people[0]
            }

            if (randomNumber < individual.fitness)
                survivors.remove(individual)

            if (survivors.size == 2) {
                break
            }
        }


        this.people = survivors
    }

    fun breed() {
        for (i in people.size until this.size) {
            val ind1 = getRandomIndividual()
            val ind2 = getRandomIndividual()
            val child: Individual

            val geneticCut = Utilities.randomInt(0, 2)
            val mutation = Utilities.randomDouble(0.0, 100.0)

            val genes1 = Arrays.copyOfRange(ind1.color, 0, geneticCut)
            val genes2 = Arrays.copyOfRange(ind2.color, geneticCut, ind2.color.size)
            val colors = Arrays.copyOf(genes1, genes1.size + genes2.size)
            System.arraycopy(genes2, 0, colors, genes1.size, genes2.size)

            child = determineFitness(Individual(colors[0], colors[1], colors[2]))
            if (mutation <= MUTATION_PROBABILITY) {
                child.mutate()
            }
            people.add(child)
        }
        generation += 1
    }
}