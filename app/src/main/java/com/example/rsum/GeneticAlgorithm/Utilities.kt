package com.example.rsum.GeneticAlgorithm

import kotlin.random.Random

class Utilities() {

    companion object {
        fun randomDouble(inclusive: Double, exclusive: Double): Double {
            return inclusive + (exclusive - inclusive) * Random.nextDouble()
        }

        fun randomInt(inclusive: Int, exclusive: Int): Int {
            return Random.nextInt(inclusive, exclusive)
        }

        fun randomBoolean(): Boolean {
            val rand = randomInt(0, 1)
            return rand == 1
        }
    }
}