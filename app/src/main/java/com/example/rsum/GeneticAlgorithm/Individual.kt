package com.example.rsum.GeneticAlgorithm

class Individual(red: Int = 0, green: Int = 0, blue: Int = 0) {
    val color = intArrayOf(red, green, blue)
    val labColor: LAB = LAB.fromRGB(red, green, blue, 0.0)
    var fitness = 0.0

    fun mutate() {
        val randomInt = Utilities.randomInt(0, 2)
        val randomAmount = Utilities.randomInt(0, 10)

        if (Utilities.randomBoolean()) {
            if (color[randomInt] - randomAmount < 0) {
                color[randomInt] = 0
            } else {
                color[randomInt] -= randomAmount
            }
        } else {
            if (color[randomInt] + randomAmount > 255) {
                color[randomInt] = 255
            } else {
                color[randomInt] += randomAmount
            }
        }
    }
}