package com.example.rsum.GeneticAlgorithm

import android.graphics.Color
import android.os.AsyncTask
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_visual.view.*

class GeneticAlgorithm(var population: Population, var timeTextView: TextView,
                       var generationTextView: TextView, var adaptabilityTextView: TextView,
                       var bestIndividualPanel: LinearLayout): AsyncTask<Void, Void, Int>() {
    var TIME_LIMIT = 5000
    var elapsedTime: Long = 0

    override fun doInBackground(vararg params: Void?): Int {
        val initialTime = System.currentTimeMillis()
        while (population.bestIndividual.fitness >= 2 && elapsedTime < TIME_LIMIT) {
            population.purge()
            population.breed()
            elapsedTime = System.currentTimeMillis() - initialTime
            publishProgress()
            Thread.sleep(50)
        }
        return 1
    }

    override fun onProgressUpdate(vararg values: Void?) {
        super.onProgressUpdate(*values)

        updateTime(elapsedTime, timeTextView)
        updateAdaptability(100 - population.bestIndividual.fitness, adaptabilityTextView)
        updateGeneration(population.generation, generationTextView)
        var bestColor = population.bestIndividual.color
        bestIndividualPanel.setBackgroundColor(Color.rgb(bestColor[0], bestColor[1], bestColor[2]))
        var color = population.bestIndividual.color
        if (LAB.ciede2000(LAB.fromRGB(color[0], color[1], color[2], 0.0), LAB.fromRGB(0, 0, 0, 0.0)) < 50) {
            bestIndividualPanel.melhorIndividuoTextView.setTextColor(Color.rgb(255, 255, 255))
        } else {
            bestIndividualPanel.melhorIndividuoTextView.setTextColor(Color.rgb(0, 0, 0))
        }
    }

    fun updateTime(time: Long, timeTextView: TextView) {
        timeTextView.text = time.toString()
    }

    fun updateGeneration(generation: Int, generationTextView: TextView){
        generationTextView.text = generation.toString()
    }

    fun updateAdaptability(adaptability: Double, adaptabilityTextView: TextView) {
        adaptabilityTextView.text = adaptability.format(2).toString()
    }
    fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)
}